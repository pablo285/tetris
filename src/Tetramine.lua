--class representing tetramine in play or preview

Tetramine = Class()

-- generate tetramine of given type, random type if type is nil
-- Types: O, I, J, L, Z, S
function Tetramine:init(type,color)

    self.tiles = {}
    self.type = type or math.random(7)
    self.tileMap = self:getTileMap(self.type)
    self.gridX = math.floor((GRID_WIDTH-#self.tileMap)/2)
    self.gridY = 1 
    self.color = color or 3
    self:generateTiles()
end

function Tetramine:render()
     --iterate through all tiles and render
    for columns in pairs(self.tiles) do
        for rows in pairs(self.tiles[columns]) do
            self.tiles[columns][rows]:render()
        end
    end
end

function Tetramine:generateTiles()
    --clear tiles from old position
    self.tiles={}
    for i=1,#self.tileMap do
        self.tiles[i] = {}
        for j=1,#self.tileMap[1] do
            --generate full/empty tiles based on tileMap
            --(math.floor for integer division)
            self.tiles[i][j] = Tile(i-1+self.gridX,j-1+self.gridY,self.tileMap[i][j],self.color)
        end
    end
end

function Tetramine:getTileMap(type)
    local tileBluePrints = {}
    -- O
    tileBluePrints[1] = {
        {'full','full'},
        {'full','full'}
    }
    -- I
    tileBluePrints[2] = {
        {'full','empty'},
        {'full','empty'},
        {'full','empty'},
        {'full','empty'}
    }
    -- J
    tileBluePrints[3] = {
        {'empty','full','empty'},
        {'empty','full','empty'},
        {'full','full','empty'}
    }
    --L
    tileBluePrints[4] = {
        {'full','full','empty'},
        {'empty','full','empty'},
        {'empty','full','empty'}
    }
    -- T
    tileBluePrints[5] = {
        {'empty','full','empty'},
        {'full','full','empty'},
        {'empty','full','empty'},        
    }
    -- S
    tileBluePrints[6] = {
        {'full','empty','empty'},
        {'full','full','empty'},
        {'empty','full','empty'},    
    }
    -- Z
    tileBluePrints[7] = {
        {'empty','full','empty'},
        {'full','full','empty'},
        {'full','empty','empty'},        
    }
    return tileBluePrints[type]
end

--MOVING FUNCTIONS
-- rotate via manipulate tileMap and regenerate tiles in new position
function Tetramine:rotate()
    self.tileMap = rotateMatrixCW(self.tileMap)
    -- simulate center rotation for I tile
    if #self.tileMap < #self.tiles then
        self.gridX = self.gridX + 1
        self.gridY = self.gridY - 1
    else if #self.tileMap > #self.tiles then
        self.gridX = self.gridX - 1
        self.gridY = self.gridY + 1
    end

    end
    self:generateTiles()
end

function Tetramine:moveDown()
        self.gridY = self.gridY + 1
        self:generateTiles()
end

function Tetramine:moveLeft()
        self.gridX = self.gridX - 1
        self:generateTiles()
end

function Tetramine:moveRight()
        self.gridX = self.gridX + 1
        self:generateTiles()    
end