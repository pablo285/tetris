-- Class representing the playing grid

Grid = Class{}

-- generate empty grid
-- optionally fill bottom #preFillRows with random tiles
function Grid:init(preFillRows)
    local preFillRows = preFillRows or 0
    --table of tiles for tracking empty/full tiles
    self.tiles = {}
    --fill with empty tiles
    for i=1,GRID_WIDTH do
        -- create empty row (2d array represented as nested table)
        self.tiles[i] = {}
        for j=1,GRID_HEIGHT do
            if j <= GRID_HEIGHT - preFillRows then 
                self.tiles[i][j] = Tile(i,j,'empty')
            else
                if math.random(GRID_WIDTH) <= 0.4*GRID_WIDTH then
                    self.tiles[i][j] = Tile(i,j,'autoFill',8)
                else 
                    self.tiles[i][j] = Tile(i,j,'empty')
                end
            end
        end
    end
end

function Grid:render()
    self:drawEmpty()
    --iterate through all tiles and render
    for columns in pairs(self.tiles) do
        for rows in pairs(self.tiles[columns]) do
            self.tiles[columns][rows]:render()
        end
    end
    
end

--draws empty playing grid
function Grid:drawEmpty(color,bgcolor)
    local color = color or 3
    local bgcolor = bgcolor or 14
    setPaletteColor(bgcolor,0.85)
    love.graphics.rectangle('fill',GRID_OFFSET,GRID_OFFSET,TILE_SIZE*GRID_WIDTH,TILE_SIZE*GRID_HEIGHT)
    for i=0,GRID_HEIGHT - 1 do
        setPaletteColor(color,0.2)
        love.graphics.rectangle('line',GRID_OFFSET,GRID_OFFSET+i*TILE_SIZE,TILE_SIZE*GRID_WIDTH,TILE_SIZE)
    end
    for i=0,GRID_WIDTH - 1 do
        love.graphics.rectangle('line',GRID_OFFSET+i*TILE_SIZE,GRID_OFFSET,TILE_SIZE,GRID_HEIGHT*TILE_SIZE)
    end
    --reset color to white
    setPaletteColor(3,1)
end

--incorporate fallen tetramine to grid
function Grid:consume(tetramine)
    for columns in pairs(tetramine.tiles)do
        for rows in pairs(tetramine.tiles[1]) do
            local currentTile = tetramine.tiles[columns][rows]
            if currentTile.type ~= 'empty' then
                self.tiles[currentTile.gridX][currentTile.gridY] = currentTile
            end
        end
    end
end

--check if there is a conflict in new mine position
function Grid:hasCollision(tetramine,gridX,gridY)
    -- if no position given, take current position
    local gridX = gridX or tetramine.gridX
    local gridY = gridY or tetramine.gridY   
        for columns in pairs(tetramine.tiles) do
            for rows in pairs(tetramine.tiles[columns]) do
                if (gridX + columns - 1 < 1 or 
                    gridY + rows - 1 < 1 or
                    gridX + columns - 1 > GRID_WIDTH or 
                    gridY + rows - 1 > GRID_HEIGHT or
                    self.tiles[gridX+columns-1][gridY+rows-1].type ~= 'empty') and 
                tetramine.tiles[columns][rows].type ~= 'empty' then
                    return true
                end
            end
        end
    -- end
    --no conflict detected
    return false
end

-- detect and remove full lines after mine lands
-- returns number of deleted full lines
function Grid:deleteFullLines()
    --start from the bottom
    local activeY = GRID_HEIGHT
    local fullLinesCount = 0
    
    repeat
        local lineFull = true
        for activeX = 1,GRID_WIDTH do
            if self.tiles[activeX][activeY].type == 'empty' then
                lineFull = false
                break
            end
        end
        if lineFull then
            fullLinesCount = fullLinesCount + 1
            --go from current line up and copy all lines one down to replace the full line
            for i = activeY,2,-1 do
                for activeX = 1,GRID_WIDTH do 
                    self.tiles[activeX][i].type = self.tiles[activeX][i-1].type
                end
            end
            --copied tiles have original gridX,gridY parameters
            --self:generateGridPositions()
        else
            activeY = activeY - 1
        end
    until activeY == 1
    return fullLinesCount
end

-- generate gridX, gridY parameters from current position in self.tiles table
function Grid:generateGridPositions()
    for columns in pairs(self.tiles) do
        for rows in pairs(self.tiles[columns]) do
            self.tiles[columns][rows].gridX = columns
            self.tiles[columns][rows].gridY = rows
        end
    end
end

-- determine if there are any prefilled tiles in the grid
function Grid:hasPrefilledTiles()
    for columns in pairs(self.tiles) do
        for rows in pairs(self.tiles[columns]) do
            if self.tiles[columns][rows].type == 'autoFill' then
                return true
            end
        end
    end
    return false
end