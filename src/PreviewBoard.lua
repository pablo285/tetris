-- class for drawing a PreviewBoard

PreviewBoard = Class{}

function PreviewBoard:init()
    self.nextMine = Tetramine()
    self.width = 100
    self.height = 100
    -- place PreviewBoard in between grid and screen edge
    self.renderX = GAME_WIDTH - math.floor((GAME_WIDTH-GRID_OFFSET-TILE_SIZE*GRID_WIDTH - self.width)/2) - self.width
    -- line up top of PreviewBoard with bottom of grid
    self.renderY = GRID_OFFSET
    -- artificial grid coordinates to center next mine in preview board
    self.nextMine.gridX = GRID_WIDTH + (self.renderX - GRID_OFFSET - GRID_WIDTH*TILE_SIZE) / TILE_SIZE + 4 - #self.nextMine.tiles / 2
    self.nextMine.gridY = 5.5 - #self.nextMine.tiles[1] / 2
    self.nextMine:generateTiles()
    self.textOffset = 10
    self.lineHeight = 25
end

function PreviewBoard:renderBackground(color,bgcolor)
    local color = color or 3
    local bgcolor = bgcolor or 1
    setPaletteColor(bgcolor,0.8)
    local textOffset = 10
    love.graphics.rectangle('fill',self.renderX, self.renderY, self.width, self.height)
    -- reset color to white
    setPaletteColor(3,1)
end

function PreviewBoard:renderText(textColor)
    local textColor = textColor or 3
    setPaletteColor(textColor,1)
    love.graphics.setFont(gFonts['medium'])
    love.graphics.printf('Next mine', self.renderX+self.textOffset, self.renderY+self.textOffset, self.width-2*self.textOffset,'left')
    -- reset color to white
    setPaletteColor(3,1)
end

function PreviewBoard:renderMine()
    self.nextMine:render()
end

function PreviewBoard:render()
    self:renderBackground()
    self:renderText() 
    self:renderMine()   
end