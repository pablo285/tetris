-- Sounds, graphics, fonts

gSounds = {
    ['music'] = love.audio.newSource('audio/bg_loop.mp3','stream'),
    ['menu'] = love.audio.newSource('audio/select.wav','static'),
    ['fail'] = love.audio.newSource('audio/fail.mp3','static'),
    ['rotate'] = love.audio.newSource('audio/rotate.mp3','static'),
    ['move'] = love.audio.newSource('audio/move.mp3','static'),
    ['pause_in'] = love.audio.newSource('audio/pause_in.mp3','static'),
    ['pause_out'] = love.audio.newSource('audio/pause_out.mp3','static'),
    ['line'] = love.audio.newSource('audio/line.mp3','static'),
    ['land'] = love.audio.newSource('audio/land.mp3','static')
}

gTextures = {
    ['background'] = love.graphics.newImage('graphics/background5.png'),
    ['tiles'] = love.graphics.newImage('graphics/arcadArne_sheet1.png')
}

gQuads = {
    ['tile'] = love.graphics.newQuad(268,148,16,16,gTextures['tiles']:getDimensions())
}

function generateTileQuads()
    for i = 1, 25 do
        gQuads[i] = love.graphics.newQuad(268,28 + i*40,16,16,gTextures['tiles']:getDimensions())
    end
end

gFonts = {
    ['small'] = love.graphics.newFont('fonts/font.ttf', 8),
    ['medium'] = love.graphics.newFont('fonts/font.ttf', 16),
    ['large'] = love.graphics.newFont('fonts/font.ttf', 32)
}
