-- class for drawing a scoreboard

ScoreBoard = Class{}

function ScoreBoard:init(score, level, lineCount)
    self.width = 100
    self.height = 160
    -- place scoreboard in between grid and screen edge
    self.renderX = GAME_WIDTH - math.floor((GAME_WIDTH-GRID_OFFSET-TILE_SIZE*GRID_WIDTH - self.width)/2) - self.width
    -- line up bottom of scoreboard with bottom of grid
    self.renderY = GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-self.height
    self.score = score or 0
    self.level = level or 0
    self.lines = lines or 0
    self.textOffset = 10
    self.lineHeight = 25
end

function ScoreBoard:renderBackground(color,bgcolor)
    local color = color or 3
    local bgcolor = bgcolor or 1
    setPaletteColor(bgcolor,0.8)
    -- local scoreBoardX = 200
    -- local scoreBoardY = GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-150
    local textOffset = 10
    love.graphics.rectangle('fill',self.renderX, self.renderY, self.width, self.height)
    -- reset color to white
    setPaletteColor(3,1)
end

function ScoreBoard:renderText(textColor)
    local textColor = textColor or 3
    setPaletteColor(textColor,1)
    love.graphics.setFont(gFonts['medium'])
    love.graphics.printf('Score', self.renderX+self.textOffset, self.renderY+self.textOffset, self.width-2*self.textOffset,'left')
    love.graphics.printf(tostring(self.score), self.renderX+self.textOffset, self.renderY+self.lineHeight+self.textOffset, self.width-2*self.textOffset,'right')
    love.graphics.printf('Level' , self.renderX+self.textOffset, self.renderY+2*self.lineHeight+self.textOffset, self.width-2*self.textOffset,'left')
    love.graphics.printf(tostring(self.level), self.renderX+self.textOffset, self.renderY+3*self.lineHeight+self.textOffset, self.width-2*self.textOffset,'right')
    love.graphics.printf('Lines', self.renderX+self.textOffset, self.renderY+4*self.lineHeight+self.textOffset, self.width-2*self.textOffset,'left')
    love.graphics.printf(tostring(self.lines), self.renderX+self.textOffset, self.renderY+5*self.lineHeight+self.textOffset, self.width-2*self.textOffset,'right')
    -- reset color to white
    setPaletteColor(3,1)
end

function ScoreBoard:updateData(score, level, lines)
    self.score = score
    self.level = level
    self.lines = lines
end

function ScoreBoard:render()
    self:renderBackground()
    self:renderText()    
end