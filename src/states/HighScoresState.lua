HighScoresState = Class{__includes = BaseState}

function HighScoresState:init() 
    -- load high scores from disk (or create default)
    self.highScores = loadHighScores()
    self.grid = Grid()
    dimMusic(true)
end

function HighScoresState:enter() end
function HighScoresState:exit() end
function HighScoresState:update(dt) end

function HighScoresState:render()
    -- render empty grid
    self.grid:render()
    -- keep the game elements darker
    setPaletteColor(1,0.8)
    love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)
    setPaletteColor(3,1)
    love.graphics.setFont(gFonts['large'])
    love.graphics.printf('HIGH\nSCORES',0,20,GAME_WIDTH,'center')
    love.graphics.setFont(gFonts['medium'])
    for k in pairs(self.highScores) do
        love.graphics.printf(self.highScores[k].name..'  '..tostring(self.highScores[k].score),70,70+k*20,GAME_WIDTH,'left')
    end
    love.graphics.setFont(gFonts['small'])
    love.graphics.printf("ESC to go back to main menu",0,GAME_HEIGHT - 50,GAME_WIDTH,'center')
end

function HighScoresState:keypressed(key, scancode, isrepeat) 
    if key == 'escape' then
        gStateMachine:change('start')
    end
end

function HighScoresState:keyreleased(key, scancode) end