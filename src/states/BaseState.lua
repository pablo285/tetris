-- BaseState for all state classes to inherit

BaseState = Class{}

function BaseState:init() end
function BaseState:enter() end
function BaseState:exit() end
function BaseState:update(dt) end
function BaseState:render() end
function BaseState:keypressed(key, scancode, isrepeat) end
function BaseState:keyreleased(key, scancode) end