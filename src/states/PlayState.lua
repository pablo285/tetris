-- State where playing the game

PlayState = Class{__includes = BaseState}

function PlayState:init()
    self.level = 1
    self.score = 0
    self.lines = 0
    self.tetramine = Tetramine() 
    self.previewBoard = PreviewBoard()
    self.scoreBoard = ScoreBoard(self.score, self.level, self.lines)
    self:generateTimers(self.level) 
    self.canInput = true
    self.paused = false
    dimMusic(false)
    BG_SCROLLSPEED = 30
end

function PlayState:enter(def)
    self.mode = def.mode
    if self.mode == 'fill' then
        self.grid = Grid(self.level)
    else
        self.grid = Grid()
    end
end

function PlayState:exit()
end


-- Input handling #1
function PlayState:keypressed(key, scancode, isrepeat)
    if key == 'escape' then
        love.event.quit()
    end
    -- switch between paused and unpaused state
    if self.canInput then
        if key == 'space' and not isrepeat then
            self:pauseSwitch()
        end
    end
    -- handling of movement input
    if self.canInput and not self.paused then 
        -- Move instantly and then every SIDEMOVE_TIME seconds
        -- Reset elapsed timer each time a move starts 
        if key then self.lastkey = key end
        if key == 'left' and not isrepeat then
            self:moveMine('left')
            self.timers['left'].elapsed = 0
            self.timers['left']:register()        
        end
        if key == 'right' and not isrepeat then
            self:moveMine('right')
            self.timers['right'].elapsed = 0
            self.timers['right']:register()
        end
        if key == 'return' or key == 'kpenter' or key =='up' and not isrepeat then
            self:rotateMine()
            self.timers['rotate'].elapsed = 0
            self.timers['rotate']:register()
        end
        if key == 'down' and not isrepeat then
            self:moveMine('down')
            self.timers['downFast'].elapsed = 0
            self.timers['downFast']:register()
            --disable timer for automatic down move when down key is pressed
            self.timers['down']:remove()
        end
    end
end

-- Input handling #2
function PlayState:keyreleased(key, scancode) 
    if self.canInput and not self.paused then
        if key == 'left' then
            self.timers['left']:remove()
        end
        if key == 'right' then
            self.timers['right']:remove()
        end
        if key == 'return' or key == 'kpenter' or key =='up' then
            self.timers['rotate']:remove()
        end
        if key == 'down' then
            self.timers['downFast']:remove()
            --reenable timer for automatic down movement
            self.timers['down'].elapsed = 0
            self.timers['down']:register()
        end
    end
end

function PlayState:update(dt)
    Timer.update(dt)
end

function PlayState:render()
    self.grid:render()
    self.tetramine:render()
    self.scoreBoard:render()
    self.previewBoard:render()
    -- render PAUSE message when paused
    if self.paused then
        setPaletteColor(1,0.8)
        love.graphics.rectangle('fill',0,0,GAME_WIDTH,GAME_HEIGHT)
        setPaletteColor(3,1)
        love.graphics.setFont(gFonts['large'])
        love.graphics.printf('PAUSE',0,(GAME_HEIGHT - love.graphics.getFont():getHeight()) / 2,GAME_WIDTH,'center')
        -- reset color to white
        setPaletteColor(3,1)
    end
    
    -- -- debug GUI text
    -- setPaletteColor(3,1)
    -- love.graphics.setFont(gFonts['medium'])
    -- love.graphics.print('GridX: ' .. tostring(self.tetramine.gridX), 210, GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-150)
    -- love.graphics.print('GridY: ' .. tostring(self.tetramine.gridY), 210, GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-135)
    -- love.graphics.print('lastkey: ' .. tostring(self.lastkey), 210, GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-120)
    -- love.graphics.print('moves: ' .. tostring(self.movecount), 210, GRID_OFFSET+TILE_SIZE*GRID_HEIGHT-105)
    
end

-- Generate timers for determining speed of movement
function PlayState:generateTimers(level)
    --delete all preexisting timers
    Timer.clear()
    self.timers = {}
    local levelMultiplier = 1.1^level
    BG_SCROLLSPEED = 30 * levelMultiplier
    self.timers['left'] = Timer.every(SIDEMOVE_TIME/levelMultiplier,
        function()
            self:moveMine('left')
        end
    )
    self.timers['right'] = Timer.every(SIDEMOVE_TIME/levelMultiplier,
        function()
            self:moveMine('right')
        end
    )
    self.timers['rotate'] = Timer.every(ROTATE_TIME/levelMultiplier,
        function()
            self:rotateMine()
        end
    )
    --timer for moving down after pressing down key
    self.timers['downFast'] = Timer.every(DOWNMOVE_TIME/20/levelMultiplier,
        function()
            self:moveMine('down')
        end
    )
    --disable all timers (otherwise they would all activate)
    for k in pairs(self.timers) do
        self.timers[k]:remove()
    end
    --timer for moving down enabled from beginning
    self.timers['down'] = Timer.every(DOWNMOVE_TIME/levelMultiplier,
        function()
            self:moveMine('down')
        end
    ) 
end

function PlayState:moveMine(direction)
    if direction == 'left' then
        if not self.grid:hasCollision(self.tetramine,self.tetramine.gridX - 1, self.tetramine.gridY) then
            self.tetramine:moveLeft()
            gSounds['move']:play()
        end
    end
    if direction == 'right' then
        if not self.grid:hasCollision(self.tetramine,self.tetramine.gridX + 1, self.tetramine.gridY) then
            self.tetramine:moveRight()
            gSounds['move']:play()
        end
    end
    if direction == 'down' then
        if not self.grid:hasCollision(self.tetramine,self.tetramine.gridX, self.tetramine.gridY + 1) then
            self.tetramine:moveDown()
        else
            self:mineLand()
        end
    end
end

function PlayState:rotateMine()
    -- create a local copy to check collisions before actual rotation
    local rotated = self.tetramine:clone()
    rotated:rotate()
    local success = false
    if self.grid:hasCollision(rotated) then
        -- attempt a wall kick (up to rotated mine width/2) if there is a collision
        for kick=1,math.floor(#rotated.tiles/2) do
            if not self.grid:hasCollision(rotated,rotated.gridX-kick,rotated.gridY) then
                self.tetramine.gridX = self.tetramine.gridX - kick
                self.tetramine:rotate()
                success = true
                break
            else 
                if not self.grid:hasCollision(rotated,rotated.gridX+kick,rotated.gridY) then
                    self.tetramine.gridX = self.tetramine.gridX + kick
                    self.tetramine:rotate()
                    success = true
                    break
                end
            end
        end
    else
        self.tetramine:rotate()
        success = true
    end
    if success then
        gSounds['rotate']:play()
    else 
        gSounds['fail']:play()
    end
end

-- Handle mine landing (line deletion, scoring, levelup, gameover)
function PlayState:mineLand()
    self.grid:consume(self.tetramine)
    -- delete lines and receive number of deleted lines
    local deletedLines = self.grid:deleteFullLines()
    -- Create a new mine and refresh mine preview
    self.tetramine = Tetramine(self.previewBoard.nextMine.type)
    self.previewBoard = PreviewBoard()
    if deletedLines > 0 then
        self.score = self.score + 2^deletedLines * 50
        self.lines = self.lines + deletedLines
        if self.mode == 'classic' then
            -- levelup after 10 lines
            self.level = math.floor(self.lines / 10)
        else 
            if self.mode == 'fill' then
                -- levelup when all prefilled tiles are cleared
                if not self.grid:hasPrefilledTiles() then
                    self.level = self.level + 1
                    -- create new grid with prefilled botom lines
                    self.grid = Grid(self.level)
                    -- add bonus score for finishing level
                    self.score = self.score + 2345
                end
            end
        end
        self.scoreBoard:updateData(self.score, self.level, self.lines)
        self:generateTimers(self.level)
        gSounds['line']:play()
    else
        gSounds['land']:play()
    end
    -- require pressing 'down' key again to move tile down fast
    self.timers['downFast']:remove()
    -- Check if the new mine is has a collision with existing tiles
    if self.grid:hasCollision(self.tetramine) then
        Timer:clear()
        for k in pairs(self.timers) do
            self.timers[k]:remove()
        end
        self.canInput =  false
        gStateMachine:change('gameover',{grid=self.grid,scoreBoard=self.scoreBoard,previewBoard=self.previewBoard,score=self.score})
    end
end

function PlayState:pauseSwitch()
    if self.paused then
        self.paused = false
        gSounds['pause_out']:play()
        -- reactivate mine falling down
        self.timers['down']:register()
    else
        self.paused = true
        gSounds['pause_in']:play()
        for k in pairs(self.timers) do
            self.timers[k]:remove()
        end
    end
    dimMusic(self.paused)
end
