-- Starting screen

local positions = {}

StartState = Class{__includes = BaseState}

function StartState:init()
    -- currently selected menu item
    self.currentMenuItem = 1
    self.grid = Grid()
    -- can be classic or fill
    self.mode = "classic"

    self.menuTexts = {"START","MODE: "..self.mode,"HIGH SCORES"}
    self.colors = {3,1,1,1,1,1}
    -- Transitioning TETRIS colors
    self.colorTimer = Timer.every(0.5,
        function() 
            for i = 7,2,-1 do
                self.colors[i] = self.colors[i-1]
            end
            self.colors[1] = self.colors[#self.colors]
        end)    

    -- used to animate full-screen transition rect
    self.transitionAlpha = 0

    dimMusic(true)

    -- pause input for animations
    self.canInput = true

    
end

function StartState:enter()
end

function StartState:exit()
    -- clean up transition timer (causes bugs when omitted)
    self.tween:remove()
end

function StartState:update(dt)
    -- update Timer used for transitions and animations
    Timer.update(dt)
end

function StartState:render()
    -- render empty grid
    self.grid:render()

    -- keep the game elements darker
    setPaletteColor(1,0.8)
    love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

    self:drawTetrisText(100)
    self:drawOptions(150)

    -- draw transition rect, normally fully transparent
    setPaletteColor(1, self.transitionAlpha)
    love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

    setPaletteColor(3,1)
end

-- Input handling
function StartState:keypressed(key, scancode, isrepeat)
    if key == 'escape' then
        love.event.quit()
    end
    if self.canInput then
        if key == 'return' or key == 'kpenter' then
            self:menuSelect(self.currentMenuItem)
        end
        if key == 'down' then
            self.currentMenuItem = self.currentMenuItem % #self.menuTexts + 1
            gSounds['menu']:play()
        end
        if key == 'up' then
            self.currentMenuItem = self.currentMenuItem - 1
            if self.currentMenuItem == 0 then 
                self.currentMenuItem = #self.menuTexts
            end
            gSounds['menu']:play()
        end
    end
end

function StartState:keyreleased(key, scancode) end

-- Draw animated Tetris text
function StartState:drawTetrisText(y)    
    local coloredText = {}
    local tetris = "TETRIS"
    -- table with alternating colors and letters
    for i = 1, #self.colors do
        table.insert(coloredText,getPaletteColor(self.colors[i]))
        table.insert(coloredText,tetris:sub(i,i))
    end
    local tetrisText = love.graphics.newText(gFonts['large'],coloredText)
    -- draw semi-transparent rect behind TETRIS
    setPaletteColor(3,0.5)
    love.graphics.rectangle('fill', (GAME_WIDTH - tetrisText:getWidth() - 10)/2, y - tetrisText:getHeight() - 10, tetrisText:getWidth() + 10, tetrisText:getHeight() +15, 6)
    setPaletteColor(3,1)
    love.graphics.draw(tetrisText,(GAME_WIDTH - tetrisText:getWidth())/2, y - tetrisText:getHeight())
end

-- Menu texts text over semi-transparent rectangles
function StartState:drawOptions(y)
    love.graphics.setFont(gFonts['medium'])
    for i = 1,#self.menuTexts do
        if self.currentMenuItem == i then
            setPaletteColor(3,0.9)
        else
            setPaletteColor(3,0.2)
        end   
        love.graphics.rectangle('fill',GAME_WIDTH / 2 - 110,y + 50*(i-1)-5,220,gFonts['medium']:getHeight()+10)
        if self.currentMenuItem == i then
            setPaletteColor(4,1)
        else
            setPaletteColor(2,1)
        end 
        
        love.graphics.printf(self.menuTexts[i], 0, y + 50*(i-1), GAME_WIDTH, 'center')  
    end
    setPaletteColor(4,1)
    love.graphics.setFont(gFonts['small'])
    if self.currentMenuItem == 2 then
        love.graphics.printf("Classic: 10 lines to levelup\n\nFill: clear all prefilled tiles",0, y + 50*#self.menuTexts,GAME_WIDTH,'center')
    end 
    setPaletteColor(3,1)
    love.graphics.printf("Enter to select/change options\n\nUp/Down to navigate menu\n\nESC to quit",0, GAME_HEIGHT - 50,GAME_WIDTH,'center')
end

-- actions for selecting menu items
function StartState:menuSelect(menuItem)
    if menuItem == 1 then
        self.canInput = false
        self.tween = Timer.tween(0.5, {
                            [self] = {transitionAlpha = 1}
                        }):finish(function()
                            gStateMachine:change('play',{mode = self.mode})
                        end)        
    else
        if menuItem == 2 then
            if self.mode == 'classic' then
                self.mode = 'fill'
            else 
                self.mode = 'classic'
            end
            self.menuTexts[2] = "MODE: "..self.mode
        else
            if menuItem == 3 then
                self.canInput = false
                self.tween = Timer.tween(0.5, {
                    [self] = {transitionAlpha = 1}
                }):finish(function()
                    gStateMachine:change('highscores')
                end) 
            end
        end
    end
end