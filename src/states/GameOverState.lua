-- Game Over screen and entering high score 

GameOverState = Class{__includes = BaseState}

function GameOverState:init() 
    -- individual chars of name string
    self.chars = {
        [1] = 65,
        [2] = 65,
        [3] = 65
    }
    -- char we're currently changing
    self.highlightedChar = 1
    -- load highscores from disk (or create default)
    self.highScores = loadHighScores()
    dimMusic(true)
end

function GameOverState:enter(def) 
    -- keep state of game from playstate for display
    self.score = def.score
    self.grid = def.grid
    self.scoreBoard = def.scoreBoard
    self.previewBoard = def.previewBoard
end
function GameOverState:exit() end
function GameOverState:update(dt) end

function GameOverState:render()
    -- static game elements from finished game
    self.grid:render()
    self.scoreBoard:render()
    self.previewBoard:render()
    -- keep the game elements darker
    setPaletteColor(1,0.9)
    love.graphics.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)
    setPaletteColor(3,1)
    love.graphics.setFont(gFonts['large'])
    love.graphics.printf("GAME OVER",0,100,GAME_WIDTH,'center')
    love.graphics.setFont(gFonts['small'])
    love.graphics.printf("ESC to go back to main menu",0,GAME_HEIGHT - 50,GAME_WIDTH,'center')
    if self.score > self.highScores[#self.highScores].score then
        self:enterHighScores()
    end
end

-- Input handling
function GameOverState:keypressed(key, scancode, isrepeat) 
    if key == 'escape' then
        gStateMachine:change('start')
    end
    -- Save score and proceed to high score display
    if key == 'return' or key == 'kpenter' then
        local name = string.char(self.chars[1]) .. string.char(self.chars[2]) .. string.char(self.chars[3])
        self:saveHighScores(name)
        gStateMachine:change('highscores')
    end
    --change highlighted char
    if key == 'left' then
        self.highlightedChar = self.highlightedChar - 1
        if self.highlightedChar < 1 then
            self.highlightedChar = 3
        end
    end
    if key == 'right' then
        self.highlightedChar = self.highlightedChar%3 + 1
    end
    -- change name of player
    if key == 'up' then
        self.chars[self.highlightedChar] = self.chars[self.highlightedChar] - 1
        if self.chars[self.highlightedChar] < 65 then
            self.chars[self.highlightedChar] = 90
        end
    end
    if key == 'down' then
        self.chars[self.highlightedChar] = self.chars[self.highlightedChar] + 1
        if self.chars[self.highlightedChar] > 90 then
            self.chars[self.highlightedChar] = 65
        end
    end
end

function GameOverState:keyreleased(key, scancode) end

-- input name to be saved with highscore
function GameOverState:enterHighScores()
    love.graphics.setFont(gFonts['medium'])
    -- prepare string with highlighted char
    local coloredText = {}
    for i = 1,3 do
        if self.highlightedChar == i then
            table.insert(coloredText,getPaletteColor(3))
        else
            table.insert(coloredText,getPaletteColor(10))
        end
        table.insert(coloredText,' '..string.char(self.chars[i]).." ")
    end
    love.graphics.printf("High score reached:\n\n"..tostring(self.score).."\n\nEnter name",0,150,GAME_WIDTH,'center')
    love.graphics.printf(coloredText,0,250,GAME_WIDTH,'center')
    setPaletteColor(3,1)
    love.graphics.setFont(gFonts['small'])
    love.graphics.printf("Up/Down/Left/Right to change name\nENTER to save score",0,GAME_HEIGHT - 80,GAME_WIDTH,'center')

end

-- save high scores to disk (love saves directory)
function GameOverState:saveHighScores(name)
    love.filesystem.setIdentity('tetris')
    -- Add new highscore to table
    table.insert(self.highScores,{name=name,score=self.score})
    -- Sort highscores by value
    table.sort(self.highScores, function(lhs, rhs) return lhs.score > rhs.score end)
    -- Remove lowest highscore
    table.remove(self.highScores)
    -- transform highscores table to string
    local scoreString = ''
    for k in pairs(self.highScores) do
        scoreString = scoreString..self.highScores[k].name..tostring(self.highScores[k].score)..'\n'
    end
    love.filesystem.write('scores.lst',scoreString)
end