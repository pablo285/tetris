--rotate 2d matrix counterclockwise
function rotateMatrix(matrix)
    local res = {}
 
    for i = 1, #matrix[1] do
        res[i] = {}
        for j = 1, #matrix do
            res[i][j] = matrix[#matrix-j+1][i]
        end
    end
    return res
end

function rotateMatrixCW(matrix)
    local res = matrix
 
    for i = 1, 3 do
        res = rotateMatrix(res)
    end
    return res
end

-- gets colors from predefined 16color palette
function getPaletteColor(colorIndex)
    local paletteColors = {
        {0, 0, 0},
        {0.615686274509804, 0.615686274509804, 0.615686274509804},
        {1, 1, 1},
        {0.745098039215686, 0.149019607843137, 0.2},
        {0.87843137254902, 0.435294117647059, 0.545098039215686},
        {0.286274509803922, 0.235294117647059, 0.168627450980392},
        {0.643137254901961, 0.392156862745098, 0.133333333333333},
        {0.92156862745098, 0.537254901960784, 0.192156862745098},
        {0.968627450980392, 0.886274509803921, 0.419607843137255},
        {0.184313725490196, 0.282352941176471, 0.305882352941176},
        {0.266666666666667, 0.537254901960784, 0.101960784313725},
        {0.63921568627451, 0.807843137254902, 0.152941176470588},
        {0.105882352941176, 0.149019607843137, 0.196078431372549},
        {0, 0.341176470588235, 0.517647058823529},
        {0.192156862745098, 0.635294117647059, 0.949019607843137},
        {0.698039215686274, 0.862745098039216, 0.937254901960784}
    }
    return paletteColors[colorIndex]
end

--sets colors in predefined 16color palette
function setPaletteColor(colorIndex, alpha)
    local alpha = alpha or 1
    local rgba = getPaletteColor(colorIndex)
    table.insert(rgba,alpha)
    love.graphics.setColor(rgba)
end

-- dimming the background track when not playing (start, pause, gameover)
function dimMusic(dim)
    if dim then
        gSounds['music']:setEffect('distortion')
        gSounds['music']:setFilter({type = 'lowpass',volume = 0.15})
    else
        gSounds['music']:setFilter()
        gSounds['music']:setEffect('distortion',false)
    end
end

function loadHighScores()
    love.filesystem.setIdentity('tetris')

    if not love.filesystem.getInfo('scores.lst') then
        local defaultScores = ''
        for i = 10, 1, -1 do
            defaultScores = defaultScores .. 'XXX'.. tostring(i-1 * 1000) .. '\n'
        end

        love.filesystem.write('scores.lst', defaultScores)
    end

    local scores = {}

    -- iterate over each line in the file, filling in names and scores
    -- first three letters on line are name, the rest is score
    for line in love.filesystem.lines('scores.lst') do
        table.insert(scores,{name=string.sub(line, 1, 3),score=tonumber(string.sub(line, 4))})
    end
    return scores
end


function saveHighScores(scores)
    -- clean the file
    love.filesystem.write('scores.lst','')
    -- write current score list
    for k in pairs(scores) do
        love.filesystem.append('scores.lst',scores[k].name..tostring(scores[k].score..'\n'))
    end
end
