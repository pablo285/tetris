--
-- StateMachine

-- States are added with a string identifier and an intialisation function.
-- It is expected the init function, will return a table with
-- Render, Update, Enter and Exit methods.
-- Usage:
--
-- gStateMachine = StateMachine {
-- 		['MainMenu'] = function()
-- 			return MainMenu()
-- 		end,
-- }
-- gStateMachine:change("MainGame")

StateMachine = Class{}

function StateMachine:init(states)
	self.empty = {
		render = function() end,
		update = function() end,
		enter = function() end,
		exit = function() end
	}
	self.states = states or {} -- [name] -> [function that returns states]
	self.current = self.empty
end

function StateMachine:change(stateName,params)
	assert(self.states[stateName]) -- state must exist!
	self.current:exit()
	self.current = self.states[stateName]()
	self.current:enter(params) --pass args to next state
end

function StateMachine:update(dt)
	self.current:update(dt)
end

function StateMachine:render()
	self.current:render()
end

function StateMachine:keypressed(key, scancode, isrepeat) 
	self.current:keypressed(key, scancode, isrepeat) 
end

function StateMachine:keyreleased(key, scancode) 
	self.current:keyreleased(key, scancode) 
end
