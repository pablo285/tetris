-- all dependencies here to prevent cluttering up main.lua

-- resizing functionality
push = require('lib.push')

Class = require('lib.class')

-- used for timers and tweening
Timer = require('lib.knife.timer')

-- Utility functions
require('src.util')

require('src.constants')

-- audio, graphics, fonts
require('src.resources')

require('src.Tile')
require('src.Grid')
require('src.Tetramine')

require('src.ScoreBoard')
require('src.PreviewBoard')

-- state machine and all states
require('src.StateMachine')
require('src.states.PlayState')
require('src.states.StartState')
require('src.states.GameOverState')
require('src.states.HighScoresState')