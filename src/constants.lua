--Virtual resolution
GAME_WIDTH = 320    
GAME_HEIGHT = 480

BG_SCROLLSPEED = 32

WINDOW_WIDTH = GAME_WIDTH*1.5
WINDOW_HEIGHT = GAME_HEIGHT*1.5

--Grid (playing board) properties
GRID_WIDTH = 11
GRID_HEIGHT = 28
GRID_OFFSET = 10

TILE_SIZE = 16 --(pixels)

-- base values for moving timers (autodown or when key is being pressed for longer)
SIDEMOVE_TIME = 0.25
DOWNMOVE_TIME = 1
ROTATE_TIME = 2

