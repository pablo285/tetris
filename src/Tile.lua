-- Class representing a single tile in the grid or tetramine

Tile = Class{}

-- type = empty, full
function Tile:init(gridX,gridY,type,color)
    self.gridX = gridX
    self.gridY = gridY
    self.type = type
    self.color = color or 3
    self:calculateRenderPos()
end

function Tile:render()
    if self.type == 'empty' then
    else
    love.graphics.draw(gTextures['tiles'],gQuads[self.color],self.renderX,self.renderY)
    end
end

-- calculate position for rendering based on postion in grid
function Tile:calculateRenderPos()
    self.renderX = GRID_OFFSET + (self.gridX-1) * TILE_SIZE
    self.renderY = GRID_OFFSET + (self.gridY-1) * TILE_SIZE
end