-- TETRIS remake by pablo285
require('src.dependencies')


-- game initialization
function love.load()
    -- enable debugger when in debug mode
    if os.getenv("LOCAL_LUA_DEBUGGER_VSCODE") == "1" then
        require("lldebugger").start()
    end
    --resizing functionality, keeps same virtual resolution
    push:setupScreen(GAME_WIDTH, GAME_HEIGHT, WINDOW_WIDTH, WINDOW_HEIGHT, {
        fullscreen = false,
        resizable = true,
        vsync = true,
        canvas = false
    })

    math.randomseed(os.time())
    love.window.setTitle('TETRIS')
    
    love.graphics.setDefaultFilter('nearest', 'nearest')

    gSounds['music']:setLooping(true)
    gSounds['music']:setVolume(0.2)
    gSounds['music']:play()

    -- creates table of quads for drawing tiles in different colors
    generateTileQuads()


    backgroundY = -256

    -- initialize state machine with all state-returning functions
    gStateMachine = StateMachine {
        ['start'] = function() return StartState() end,
        ['play'] = function() return PlayState() end,
        ['gameover'] = function() return GameOverState() end,
        ['highscores'] = function() return HighScoresState() end
    }
    gStateMachine:change('start')

end

function love.update(dt)
    -- scroll background, used across all states
    backgroundY = backgroundY + BG_SCROLLSPEED * dt
        
    -- reset when one repeating block is scrolled
    if backgroundY >= 0 then
        backgroundY = -256
    end

    gStateMachine:update(dt)
end

function love.keypressed(key, scancode, isrepeat)
    gStateMachine:keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode)
    gStateMachine:keyreleased(key, scancode)
end

function love.resize(w,h)
    push:resize(w,h)
end

function love.draw()
    push:start()

    -- scrolling background drawn behind every state
    love.graphics.draw(gTextures['background'], 0, backgroundY)     

    gStateMachine:render()

    push:finish()
end